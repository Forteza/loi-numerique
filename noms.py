#!/usr/bin/env python
import json

import pattern.fr
import wordcloud


file = open("gouvernement.json")
data = json.load(file)
pour = {}
contre = {}
for item in data.values():
    opinion = item["opinion"]
    for argument in opinion["arguments"]:
        texte_parse = pattern.fr.parse(argument["body"])
        for phrase_parse in texte_parse.split():
            for mot_parse in phrase_parse:
                if "NN" == mot_parse[1]:
                    nom = mot_parse[0]
                    nom_singulier = pattern.fr.singularize(nom)
                    if argument["type"] == 1:
                        pour[nom_singulier] = pour.get(nom_singulier, 0) + 1
                    else:
                        contre[nom_singulier] = contre.get(nom_singulier, 0) + 1

noms_neutres = []
seuil_pour = 5
seuil_contre = 10
for nom, nombre in contre.items():
    if nombre > seuil_contre and pour.get(nom, 0) > seuil_pour:
        noms_neutres.append(nom)

liste_contre = []
for nom, nombre in contre.items():
    if nombre > seuil_contre and nom not in noms_neutres:
        liste_contre.append([nom, nombre])
liste_contre.sort(key = lambda couple: (couple[1], couple[0]))
cloud = wordcloud.WordCloud().generate_from_frequencies(liste_contre)
image = cloud.to_image()
image.save("nom-contre.png")

liste_pour = []
for nom, nombre in pour.items():
    if nombre > seuil_pour and nom not in noms_neutres:
        liste_pour.append([nom, nombre])
liste_pour.sort(key = lambda couple: (couple[1], couple[0]))
cloud = wordcloud.WordCloud().generate_from_frequencies(liste_pour)
image = cloud.to_image()
image.save("nom-pour.png")
