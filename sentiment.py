#!/usr/bin/env python
import json

import pattern.fr

file = open("gouvernement.json")
data = json.load(file)
pour = []
contre = []
for item in data.values():
    opinion = item["opinion"]
    for argument in opinion["arguments"]:
        sentiment = pattern.fr.sentiment(argument["body"])
        if argument["type"] == 1:
            pour.append(sentiment)
        else:
            contre.append(sentiment)

somme_polarite = 0
somme_subjectivite = 0
for sentiment in pour:
    polarite, subjectivite = sentiment
    somme_polarite += polarite
    somme_subjectivite += subjectivite
print somme_polarite / len(pour)
print somme_subjectivite / len(pour)

somme_polarite = 0
somme_subjectivite = 0
for sentiment in contre:
    polarite, subjectivite = sentiment
    somme_polarite += polarite
    somme_subjectivite += subjectivite
print somme_polarite / len(contre)
print somme_subjectivite / len(contre)
