#!/usr/bin/env python
import json

import pattern.fr
import wordcloud


file = open("gouvernement.json")
data = json.load(file)
pour = {}
contre = {}
for item in data.values():
    opinion = item["opinion"]
    for argument in opinion["arguments"]:
        texte_parse = pattern.fr.parse(argument["body"])
        for phrase_parse in texte_parse.split():
            for mot_parse in phrase_parse:
                if "JJ" == mot_parse[1]:
                    adjectif = mot_parse[0]
                    adjectif_normalise = pattern.fr.predicative(adjectif)
                    if argument["type"] == 1:
                        pour[adjectif_normalise] = pour.get(adjectif_normalise, 0) + 1
                    else:
                        contre[adjectif_normalise] = contre.get(adjectif_normalise, 0) + 1

adjectifs_neutres = []
seuil_pour = 2
seuil_contre = 4
for adjectif, nombre in contre.items():
    if nombre > seuil_contre and pour.get(adjectif, 0) > seuil_pour:
        adjectifs_neutres.append(adjectif)

liste_contre = []
for adjectif, nombre in contre.items():
    if len(adjectif) > 1 and nombre > seuil_contre and adjectif not in adjectifs_neutres:
        liste_contre.append([adjectif, nombre])
liste_contre.sort(key = lambda couple: (couple[1], couple[0]))
cloud = wordcloud.WordCloud().generate_from_frequencies(liste_contre)
image = cloud.to_image()
image.save("adjectif-contre.png")

liste_pour = []
for adjectif, nombre in pour.items():
    if len(adjectif) > 1 and nombre > seuil_pour and adjectif not in adjectifs_neutres:
        liste_pour.append([adjectif, nombre])
liste_pour.sort(key = lambda couple: (couple[1], couple[0]))
cloud = wordcloud.WordCloud().generate_from_frequencies(liste_pour)
image = cloud.to_image()
image.save("adjectif-pour.png")
